-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 05 Jan 2017 pada 14.40
-- Versi Server: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `carica`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `merk`
--

CREATE TABLE IF NOT EXISTS `merk` (
  `ID_Merk` int(11) NOT NULL,
  `Merk` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `merk`
--

INSERT INTO `merk` (`ID_Merk`, `Merk`) VALUES
(2, 'bromo'),
(3, 'semeru'),
(4, 'wonosobo'),
(5, 'jaya wijaya'),
(6, 'Prau');

-- --------------------------------------------------------

--
-- Struktur dari tabel `produk`
--

CREATE TABLE IF NOT EXISTS `produk` (
  `Kode_Produk` char(5) NOT NULL,
  `Merk` varchar(25) NOT NULL,
  `Netto` varchar(8) NOT NULL,
  `Isi` int(11) NOT NULL,
  `Harga` int(11) NOT NULL,
  `Gambar` varchar(100) NOT NULL,
  `Nama_Gambar` varchar(20) NOT NULL,
  `Keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `produk`
--

INSERT INTO `produk` (`Kode_Produk`, `Merk`, `Netto`, `Isi`, `Harga`, `Gambar`, `Nama_Gambar`, `Keterangan`) VALUES
('AA1', '', '200', 12, 12000, 'C:/xampp/htdocs/carica/uploads/carica.jpg', 'carica.jpg', 'Enak disantap kapan saja\r\nKapan saja\r\ndan dimana saja \r\n:)'),
('B02', 'bromo', '300', 12, 1000, 'C:/xampp/htdocs/carica/uploads/carica.png', 'carica.png', 'Dibuat dengan carica pilihan'),
('C03', 'jaya wijaya', '450', 12, 45675, 'C:/xampp/htdocs/carica/uploads/carica2.jpg', 'carica2.jpg', 'Manisnya pas'),
('D12', 'Prau', '220', 5, 12000, 'C:/xampp/htdocs/carica/uploads/chileancarica.jpg', 'chileancarica.jpg', 'Dingin, segeeerrr'),
('E33', 'semeru', '90', 24, 30000, 'C:/xampp/htdocs/carica/uploads/carica-cup-mini.jpg', 'carica-cup-mini.jpg', 'Carica kita semua'),
('G09', 'bromo', '123', 12, 23456, 'C:/xampp/htdocs/carica/uploads/buah.jpg', 'buah.jpg', 'Seger, asli Wonosobo');

-- --------------------------------------------------------

--
-- Struktur dari tabel `top`
--

CREATE TABLE IF NOT EXISTS `top` (
  `ID_Top` int(11) NOT NULL,
  `Kode_Produk` varchar(5) NOT NULL,
  `Merk` varchar(25) NOT NULL,
  `Netto` varchar(8) NOT NULL,
  `Isi` int(11) NOT NULL,
  `Harga` int(11) NOT NULL,
  `Gambar` varchar(100) NOT NULL,
  `Nama_Gambar` varchar(20) NOT NULL,
  `Keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `top`
--

INSERT INTO `top` (`ID_Top`, `Kode_Produk`, `Merk`, `Netto`, `Isi`, `Harga`, `Gambar`, `Nama_Gambar`, `Keterangan`) VALUES
(1, 'C03', 'jaya wijaya', '450', 12, 45675, 'C:/xampp/htdocs/carica/uploads/carica2.jpg', 'carica2.jpg', 'Manisnya pas'),
(2, 'B02', 'bromo', '300', 12, 1000, 'C:/xampp/htdocs/carica/uploads/carica.png', 'carica.png', 'Dibuat dengan carica pilihan'),
(3, 'D12', 'Prau', '220', 5, 12000, 'C:/xampp/htdocs/carica/uploads/chileancarica.jpg', 'chileancarica.jpg', 'Dingin, segeeerrr'),
(4, 'E33', 'semeru', '90', 24, 30000, 'C:/xampp/htdocs/carica/uploads/carica-cup-mini.jpg', 'carica-cup-mini.jpg', 'Carica kita semua'),
(5, 'AA1', 'wonosobo', '200', 12, 12000, 'C:/xampp/htdocs/carica/uploads/carica.jpg', 'carica.jpg', 'Enak disantap kapan saja');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `merk`
--
ALTER TABLE `merk`
  ADD PRIMARY KEY (`ID_Merk`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`Kode_Produk`);

--
-- Indexes for table `top`
--
ALTER TABLE `top`
  ADD PRIMARY KEY (`ID_Top`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `merk`
--
ALTER TABLE `merk`
  MODIFY `ID_Merk` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
