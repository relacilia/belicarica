            <!-- End Top Menu -->
            <!-- === END HEADER === -->
            <!-- === BEGIN CONTENT === -->
            <div id="slideshow" class="bottom-border-shadow">
                <div class="container no-padding background-white bottom-border">
                    <div class="row">
                        <!-- Carousel Slideshow -->
                        <div id="carousel-example" class="carousel slide" data-ride="carousel">
                            <!-- Carousel Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example" data-slide-to="1"></li>
                                <li data-target="#carousel-example" data-slide-to="2"></li>
                            </ol>
                            <div class="clearfix"></div>
                            <!-- End Carousel Indicators -->
                            <!-- Carousel Images -->
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img src="assets/img/slideshow/slides1.jpg" style="height:360px">
                                </div>
                                <div class="item">
                                    <img src="assets/img/slideshow/slides2.jpg" style="height:360px">
                                </div>
                                <div class="item">
                                    <img src="assets/img/slideshow/slides3.jpg" style="height:360px">
                                </div>
                                <div class="item">
                                    <img src="assets/img/slideshow/slides4.jpg" style="height:360px">
                                </div>
                            </div>
                            <!-- End Carousel Images -->
                            <!-- Carousel Controls -->
                            <a class="left carousel-control" href="#carousel-example" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                            <!-- End Carousel Controls -->
                        </div>
                        <!-- End Carousel Slideshow -->
                    </div>
                </div>
            </div>

             <div id="portfolio" class="bottom-border-shadow">
                <div class="container bottom-border">
                    <h3 style="padding-top:20px;"> Top 5 of The Most Favorite This Week</h3>
                    <div class="row padding-top-20">
                        <ul class="portfolio-group">
                            <!-- Portfolio Item -->
                            <?php foreach($query  as $r): ?>
                            <?php if ($r->ID_Top == 1) {?>

                            <li class="portfolio-item col-sm-4 col-xs-6 margin-bottom-40">
                                <a href="#">
                                    <figure class="animate fadeInLeft">
                                        <img alt="image1" src="<?php echo base_url()?>uploads/<?php echo $r->Nama_Gambar; ?>">
                                        <figcaption>
                                            <h3><?php echo $r->Merk; ?></h3>
                                            <span><?php echo $r->Keterangan; ?></span>
                                            <br>
                                            <a href="<?php echo base_url()?>index.php/welcome/detailproduk/<?php echo $r->Kode_Produk ?>"><button>LIHAT DETAIL</button></a>
                                        </figcaption>
                                    </figure>
                                </a>
                            </li>
                            <?php } ?>

                            
                            <?php if ($r->ID_Top == 2) {?>

                            <li class="portfolio-item col-sm-4 col-xs-6 margin-bottom-40">
                                <a href="#">
                                    <figure class="animate fadeIn">
                                        <img alt="image2" src="<?php echo base_url()?>uploads/<?php echo $r->Nama_Gambar; ?>">
                                        <figcaption>
                                            <h3><?php echo $r->Merk; ?></h3>
                                            <span><?php echo $r->Keterangan; ?></span>
                                            <br>
                                            <a href="<?php echo base_url()?>index.php/welcome/detailproduk/<?php echo $r->Kode_Produk ?>"><button>LIHAT DETAIL</button></a>
                                        </figcaption>
                                    </figure>
                                </a>
                            </li>

                            <?php } ?>
                            
                             <?php if ($r->ID_Top == 3) {?>
                            <li class="portfolio-item col-sm-4 col-xs-6 margin-bottom-40">
                                <a href="#">
                                    <figure class="animate fadeInRight">
                                        <img alt="image3" src="<?php echo base_url()?>uploads/<?php echo $r->Nama_Gambar; ?>">
                                        <figcaption>
                                            <h3><?php echo $r->Merk; ?></h3>
                                            <span><?php echo $r->Keterangan; ?></span>
                                            <br>
                                            <a href="<?php echo base_url()?>index.php/welcome/detailproduk/<?php echo $r->Kode_Produk ?>"><button>LIHAT DETAIL</button></a>
                                        </figcaption>
                                    </figure>
                                </a>
                            </li>
                             <?php } ?>
                            
                             <?php if ($r->ID_Top == 4) {?>
                            <li class="portfolio-item col-sm-4 col-xs-6 margin-bottom-40">
                                <a href="#">
                                    <figure class="animate fadeInLeft">
                                        <img alt="image4" src="<?php echo base_url()?>uploads/<?php echo $r->Nama_Gambar; ?>">
                                        <figcaption>
                                            <h3><?php echo $r->Merk; ?></h3>
                                            <span><?php echo $r->Keterangan; ?></span>
                                            <br>
                                            <a href="<?php echo base_url()?>index.php/welcome/detailproduk/<?php echo $r->Kode_Produk ?>"><button>LIHAT DETAIL</button></a>
                                        </figcaption>
                                    </figure>
                                </a>
                            </li>
                            <?php } ?>
                            
                            <?php if ($r->ID_Top == 5) {?>
                            <li class="portfolio-item col-sm-4 col-xs-6 margin-bottom-40">
                                <a href="#">
                                    <figure class="animate fadeIn">
                                        <img alt="image5" src="<?php echo base_url()?>uploads/<?php echo $r->Nama_Gambar; ?>">
                                        <figcaption>
                                            <h3><?php echo $r->Merk; ?></h3>
                                            <span><?php echo $r->Keterangan; ?></span>
                                            <br>
                                            <a href="<?php echo base_url()?>index.php/welcome/detailproduk/<?php echo $r->Kode_Produk ?>"><button>LIHAT DETAIL</button></a>
                                        </figcaption>
                                    </figure>
                                </a>
                            </li>
                            <?php } ?>

                            <?php endforeach; ?>
                            <!-- //Portfolio Item// -->
                            <!-- Portfolio Item -->
                            <li class="portfolio-item col-sm-4 col-xs-6 margin-bottom-40" style="position: absolute; left: 693px; bottom:180px; top:0px; margin-top:10%;">
                                <a href="#">
                                    <figure class="animate fadeInRight" style="border:0px;">
                                        <a href="<?php echo base_url()?>index.php/welcome/produk"><button type="button" class="btn btn-warning">LIHAT SELENGKAPNYA  <i class="fa fa-arrow-circle-right"></i></button></a>
                                    </figure>
                                </a>
                            </li>
                            <!-- //Portfolio Item// -->

                       </ul>
                    </div>
                </div>
            </div>
        </div>





<!-- hapus 3 ikon -->

            <div id="content" class="bottom-border-shadow">
                <div class="container background-white bottom-border">
                    <div class="row margin-vert-30">
                        <!-- Main Text -->
                        <div class="col-md-6">
                            <h3> TENTANG CARICA </h3>
                            <br>
                            <h2>Carica Dieng Wonosobo</h2>
                            <p>Carica merupakan salah satu buah yang hanya tumbuh di Dataran Tinggi Dieng Wonosobo, buah ini akan banyak anda temukan di pinggir lahan tanaman masyarakat Wonosobo.
                            </p>
                            <p>Carica atau masyarakat sering menyebutnya dengan pepaya gunung ini menjadi salah satu ikon pariwisata di Wonosobo dan menjadi oleh-oleh utama khas Wonosobo, tidak sedikit wisatawan yang suka dengan buah ini karena rasanya yang lezat dan sejuta manfaat yang didapat dari buah ini.</p>
                            <p>Carica memiliki nama latin vasconcellea cundinamarcensis biasa tumbuh pada ketinggian 1.500 hingga 3.000 di atas permukaan air laut. Mitos masyarakat menyebutkan jika buah ini hanya dapat tumbuh di Dataran Tinggi Dieng, banyak petani yang mencoba untuk menanam buah ini ditempat lain namun hasilnya bukan carica yang muncul dan ternyata berbuah pepaya</p>
                            <p>Buah carica berasal dari Dataran Tinggi Andes, Amerika Selatan, masyarakat Wonosobo sering menyebut buah ini dengan sebutan carica, karika, pepaya carica, pepaya gunung dan sebagainya, bahkan ada yang menyebutnya buah dewa karena menurut mitos yang berkembang di masyarakat bahwa buah ini adalah buah yang sering dikonsumsi oleh para Dewa yang menduduki kawasan Wisata Dieng</p>
                            <p>Buah ini ada di Dieng sejak jaman penjajahan Belanda dan anda akan menemukan buah yang hampir mirip dengan buah ini namun memiliki beda rasa di Pulau Bali, mereka sering menyebutnya dengan gedang memedi.</p>
                        </div>
                        <!-- End Main Text -->
                        <div class="col-md-6">
                            <br>
                            <br>
                            <h3 class="padding-vert-10">Manfaat Carica bagi Tubuh</h3>
                            <ul class="tick animate fadeInRight">
                                <li>Buah carica memiliki serat tinggi</li>
                                <p style="padding-left:23px;"> Berguna bagi proses pencernaan tubuh, untuk anda yang memiliki masalah terhadap proses pencernaan seperti sulit BAB bisa menggunakan carica sebagai media pengobatannya.</p>
                                <li>Enzim papain pada buah ini berguna untuk menetralkan pH dan membunuh bakteri jahat dalam usus</li>
                                <li>Kaya Vitamin A</li>
                                 <p style="padding-left:23px;">Vitamin A dalam buah ini lebih banyak daripada buah wortel, karena hal inilah manfaat carica baik untuk kesehatan mata.</p>
                                <li>Vitamin C dan E pada buah ini dapat menangkal radikal bebas dan sinar UV yang menyebabkan kerusakan pada kulit, menjaga kelembapan kulit dan kecantikan kulit, banyak ditemukan salon kecantikan di Wonosobo yang menggunakan media carica</li>
                                <li>Vitamin B pada buah ini dapat menjadi sumber tambahan energi pada tubuh karena Vitamin B berpengaruh penting dalam proses metabolisme tubuh</li>
                                <li>Carica dapat mencegah Kanker karena mengandung zat agrinin yang dapat menghambat pertumbuhan sel kanker terutama kanker payudara, dan masih banyak manfaat lainnya</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- === END CONTENT === -->
           