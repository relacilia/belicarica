            <!-- === BEGIN CONTENT === -->
             <div id="content" class="bottom-border-shadow">
                <div class="container background-white bottom-border">
                    <div class="row margin-vert-30" style="text-indent: 50px;">
                        <!-- Main Text -->
                        <div class="col-md-8">
                        <h3> 1. Pemesanan </h3>
                        <p class="a"> Pilih salah satu sarana untuk menghubungi admin. </p>
                        <!-- <img alt="contact" src="<?php echo base_url()?>assets/img/contact.png" style="margin-left: 50px;"> -->
                        <p> Kemudian kirim pesan dengan format bebas namun harap menyertakan:</p>
                        <ol style="margin-left: 50px; text-indent: 10px;">
                          <li> Nama Pemesan </li>
                          <li> Kode Produk </li>
                          <li> Banyak dus yang dipesan </li>
                          <li> Alamat </li>
                          <li> Agen Pengiriman </li>
                        </ol>
                        <p> Contoh pemesanan: </p>
                          <img alt="contact" src="<?php echo base_url()?>assets/img/contact.png" style="margin-left: 50px;">
                        <p> Admin akan membalas pesan Anda dengan menyertakan total harga Carica + ongkos kiriman. </p>
                        <h3> 2. Pembayaran </h3>
                        <p class="a"> Setelah terjadi kesepakatan harga pemesanan antara Anda dengan Admin, silahkan transfer total pembayaran ke rekening di bawah ini: </p>
                        <ol style="margin-left: 50px; text-indent: 10px;">
                          <li> BNI: </li>
                          <li> BRI: </li>
                        </ol>
                        <h3> 3. Konfirmasi Pembayaran </h3>
                        <p> Kirim Bukti Tranfer Anda kepada Admin dengan format sebagai berikut: </p>
                        <ol style="margin-left: 50px; text-indent: 10px;">
                          <li> Nama Pemesan </li>
                          <li> Nama Pemilik Rekening </li>
                          <li> Tujuan Bank </li>
                          <li> Total </li>
                        </ol>
                        <p> Contoh Konfirmasi Pembayaran: </p>
                          <img alt="contact" src="<?php echo base_url()?>assets/img/contact.png" style="margin-left: 50px; margin-bottom:10px;">
                          <br>
                        <h3> 4. Pengiriman </h3>
                        <p> Pihak kami akan melakukan pengiriman segera setelah adanya konfirmasi pembayaran. Agen pengiriman berdasarkan agen yang dipilih oleh Anda. </p>
                        <h3> 5. Retur Barang </h3>
                        <!-- End Main Text -->
                      </div>
                      <div class="col-md-4" style="text-indent: 0px;">
                        <h3>Contact Detail</h3>
                        <img alt="contact" src="<?php echo base_url()?>assets/img/contact.png" style="margin-right: 0px;">
                        <br>
                        <br>
                        <h3>Agen Pengiriman</h3>
                        <img alt="contact" src="<?php echo base_url()?>assets/img/contact.png" style="margin-right: 0px;">
                      </div>


                        </div>
                    </div>
                </div>