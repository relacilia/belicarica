            <!-- === BEGIN CONTENT === -->
             <div id="content" class="bottom-border-shadow">
                <div class="container background-white bottom-border">
                    <div class="row margin-vert-30">
                        <!-- Main Text -->
                       <?php foreach($query  as $r): ?>  
                        <div class="col-md-4">
                        <div class="panel panel-primary invert">
                            <img src="<?php echo base_url()?>uploads/<?php echo $r->Nama_Gambar; ?>" style="height:300px;">                      
                            <div class="panel-footer">
                              Kode Produk : <?php echo $r->Kode_Produk ?>
                          </div>
                          <div class="panel-footer">
                              Merk        : <?php echo $r->Merk ?>
                          </div>
                           <div class="panel-footer">
                              Netto       : <?php echo $r->Netto ?>
                          </div>
                          <div class="panel-footer">
                              Isi       : <?php echo $r->Isi ?>
                          </div>
                           <div class="panel-footer">
                              Harga       : <?php echo $r->Harga ?>
                          </div>
                          <div class="panel-footer" style="background-color: #f9a825;">
                              <a href="<?php echo base_url()?>index.php/welcome/detailproduk/<?php echo $r->Kode_Produk ?>" style="color:#000">SELENGKAPNYA <i class="fa fa-arrow-circle-right"></i>
                          </a>
                          </div>
                            </div>
                        </div>
                       <?php endforeach; ?>
                        <!-- End Main Text -->

                        </div>
                    </div>
                </div>