    <div id="content" class="bottom-border-shadow">
                <div class="container background-white bottom-border">
                  <br>
                  <a href="<?php echo base_url()?>index.php/welcome/produk"><button style="background:orange"><span class="glyphicon glyphicon-chevron-left" style="color:white;"> BACK</span></button>

                    <div class="row margin-vert-30">
                        <!-- Main Text -->
                        <div class="col-md-4">
                        <div class="panel panel-primary invert">
                            <img alt="image5" src="<?php echo base_url()?>uploads/<?php echo $query->Nama_Gambar; ?>" >                      
                            <div class="panel-footer">
                              Kode Produk : <?php echo $query->Kode_Produk ?>
                          </div>
                          <div class="panel-footer">
                              Merk        : <?php echo $query->Merk ?>
                          </div>
                           <div class="panel-footer">
                              Netto       : <?php echo $query->Netto ?>
                          </div>
                           <div class="panel-footer">
                              Harga       : <?php echo $query->Harga ?>
                          </div>
                
                            </div>
                        </div>
                        <!-- End Main Text -->

                        <div class="col-md-8">
                            <div class="panel panel-primary">
                              <div class="panel-heading">
                                <h4 class="panel-title" style="font-weight:bolder">Review</h4>
                              </div>
                                <div class="panel-body">
                                  <?php echo $query->Keterangan ?>
                              </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <h3> Recomendasi Carica lainnya </h3>
                            <div id="carousel-example-2" class="carousel slide alternative" data-ride="carousel">
                              <!-- Indicators -->
                              <ol class="carousel-indicators">
                                <li data-target="#carousel-example-2" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example-2" data-slide-to="1"></li>
                              </ol>
                              <!-- Wrapper for slides -->
                              <div class="carousel-inner">
                                <div class="item active">
                                  <div class="row">
                                    <div class="col-md-3"><img src="<?php echo base_url()?>assets/img/thumbsgallery/image01.jpg" style="max-width:100%;"></div>
                                    <div class="col-md-3"><img src="<?php echo base_url()?>assets/img/thumbsgallery/image02.jpg" style="max-width:100%;"></div>
                                    <div class="col-md-3"><img src="<?php echo base_url()?>assets/img/thumbsgallery/image03.jpg" style="max-width:100%;"></div>
                                    <div class="col-md-3"><img src="<?php echo base_url()?>assets/img/thumbsgallery/image04.jpg" style="max-width:100%;"></div>
                                  </div>
                                </div>
                                <div class="item">
                                  <div class="row">
                                    <div class="col-md-3"><img src="<?php echo base_url()?>assets/img/thumbsgallery/image05.jpg" style="max-width:100%;"></div>
                                    <div class="col-md-3"><img src="<?php echo base_url()?>assets/img/thumbsgallery/image06.jpg" style="max-width:100%;"></div>
                                    <div class="col-md-3"><img src="<?php echo base_url()?>assets/img/thumbsgallery/image07.jpg" style="max-width:100%;"></div>
                                    <div class="col-md-3"><img src="<?php echo base_url()?>assets/img/thumbsgallery/image08.jpg" style="max-width:100%;"></div>
                                  </div>
                                </div>
                              </div>
                              <!-- Controls -->
                              <a class="left carousel-control" href="#carousel-example-2" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                              </a>
                              <a class="right carousel-control" href="#carousel-example-2" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                              </a>
                            </div>
                        </div>



                        </div>
                    </div>
                </div>