 <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">List Produk</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default" style="border-color:#FFF;box-shadow: 0 1px 1px rgba(0, 0, 0, 0)">
                        <a href="<?php echo base_url()?>index.php/admin/tambahproduk"><button type="button" class="btn btn-success">Tambah Produk</button></a>

                        <a href="<?php echo base_url()?>index.php/admin/tambahmerk"><button type="button" class="btn btn-info">Tambah Merk</button></a>
                    </div>
                </div>
            </div>


             <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                       <!--  <div class="panel-heading">
                            DataTables Advanced Tables
                        </div> -->
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Kode Produk</th>
                                            <th>Merk</th>
                                            <th>Netto</th>
                                            <th>Isi @kardus</th>
                                            <th>Harga</th>
                                            <th>Edit</th>
                                            <th>Hapus</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($query  as $r): ?>
                                             <tr>
                                                <td><?php echo $r->Kode_Produk; ?> </td>
                                                <td> <?php echo $r->Merk ?></td>
                                                <td> <?php echo $r->Netto ?> </td>
                                                <td class="center">  <?php echo $r->Isi ?></td>
                                                <td class="center">  <?php echo $r->Harga ?></td>
                                                <td class="center" style="text-align:center;"><a href="<?php echo base_url()?>index.php/admin/editproduk/<?php echo $r->Kode_Produk ?>"><button type="button" class="btn btn-warning">Edit</button></a></td>
                                                <td class="center" style="text-align:center;"><a href="<?php echo base_url()?>index.php/admin/hapus/<?php echo $r->Kode_Produk ?>" class="cd-popup-trigger"><button type="button" class="btn btn-danger">Hapus</button></a></td>

                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->