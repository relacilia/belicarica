<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Tambah Merk</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- <div class="panel-heading">
                            Basic Form Elements
                        </div> -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <?php echo form_open_multipart('upload/addmerk');?>
                                    
                                        <div class="form-group" >
                                            <label>Merk</label>
                                            <input class="form-control" name="merky">
                                            <p class="help-block">Silahkan masukkan nama merk Carica</p>
                                        </div>
                                        
                                         <button type="submit" class="btn btn-success">SIMPAN </button>
                                       
                                    </form>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
             
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                        </div>

                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->