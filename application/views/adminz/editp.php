        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Edit Produk</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- <div class="panel-heading">
                            Basic Form Elements
                        </div> -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <!-- <form role="form" action="<?php echo base_url()?>index.php/upload/do_upload" method="post" enctype="multipart/form-data"> -->
                                        <?php echo form_open_multipart('upload/do_update/'.$pro->Kode_Produk);?>
                                       
                                        <div class="form-group">
                                            <label>Kode Produk</label>
                                            <input class="form-control" value="<?php echo $pro->Kode_Produk; ?>" name="kode" disabled>
                                        </div>

                                        <div class="form-group">
                                            <label>Merk</label>
                                            <select class="form-control" value="<?php echo $pro->Merk; ?>" name="merk">
                                                <?php foreach($query  as $r): ?>
                                                <option selected disabled hidden>Pilih Merk</option>
                                                <option><?php echo $r->Merk ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                            <p class="help-block" style="color:red">Wajib diisi kembali!</p>
                                        </div>

                                        <div class="form-group">
                                            <label>Netto</label>
                                            <input class="form-control" value="<?php echo $pro->Netto; ?>" name="netto">
                                            <p class="help-block">Example block-level help text here.</p>
                                        </div>

                                        <div class="form-group">
                                            <label>Isi @dus</label>
                                            <input class="form-control" value="<?php echo $pro->Isi; ?>" name="isi">
                                            <p class="help-block">Example block-level help text here.</p>
                                        </div>

                                        <div class="form-group">
                                            <label>Harga</label>
                                            <input class="form-control" value="<?php echo $pro->Harga; ?>" name="harga">
                                            <p class="help-block">Example block-level help text here.</p>
                                        </div>

                                        <div class="form-group">
                                            <label>Ubah Keterangan</label>
                                            <textarea class="form-control" value="<?php echo $pro->Keterangan; ?>" name="ket" rows="3"></textarea>
                                        </div>


                                        <div class="form-group">
                                            <label>Ubah Gambar</label>
                                            <input type="file" name="xyz" >
                                            <input type="hidden" name="imagez" value="<?php echo $pro->Gambar; ?>">
                                        </div>
                                        
                                         <button type="submit" class="btn btn-success">SIMPAN </button>
                                        
                                    </form>
                                </div>

                                <div class="col-lg-6">
                                    <div class="panel-body">
                                    <img src="<?php echo base_url()?>uploads/<?php echo $pro->Nama_Gambar; ?>" style="width:480px">
                                </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="well well-sm"> 
                                    <label>Keterangan</label>  
                                    <p><?php echo $pro->Keterangan ?></p>
                                </div>
                                </div>
                                
                            </div>
                          
                        </div>

                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>