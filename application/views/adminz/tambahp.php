        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Tambah Produk</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <!-- <div class="panel-heading">
                            Basic Form Elements
                        </div> -->
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <!-- <form role="form" action="<?php echo base_url()?>index.php/upload/do_upload" method="post" enctype="multipart/form-data"> -->
                                        <?php echo form_open_multipart('upload/do_upload');?>
                                        <div class="form-group">
                                            <label>Kode Produk</label>
                                            <input class="form-control" placeholder="Enter text" name="kode">
                                            <p class="help-block">Maksimal 5 karakter.</p>
                                        </div>

                                        <div class="form-group">
                                            <label>Merk</label>
                                            <select class="form-control" name="merk">
                                                <?php foreach($query  as $r): ?>
                                                <option><?php echo $r->Merk ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Netto</label>
                                            <input class="form-control" name="netto">
                                            <p class="help-block">Example block-level help text here.</p>
                                        </div>

                                        <div class="form-group">
                                            <label>Isi @dus</label>
                                            <input class="form-control" name="isi">
                                            <p class="help-block">Example block-level help text here.</p>
                                        </div>

                                        <div class="form-group">
                                            <label>Harga</label>
                                            <input class="form-control" name="harga">
                                            <p class="help-block">Example block-level help text here.</p>
                                        </div>

                                        <div class="form-group">
                                            <label>Keterangan</label>
                                            <textarea class="form-control" name="ket" rows="3"></textarea>
                                        </div>

                                        

                                        <div class="form-group">
                                            <label>File input</label>
                                            <input type="file" name="xyz">
                                        </div>
                                        
                                         <button type="submit" class="btn btn-success">SIMPAN </button>
                                       
                                    </form>
                                </div>
                            </div>
                          
                        </div>

                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>