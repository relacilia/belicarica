<!--=== BEGIN HEADER === -->
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <head>
        <!-- Title -->
        <title>Habitat - A Professional Bootstrap Template</title>
        <!-- Meta -->
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <!-- Favicon -->
        <link href="favicon.ico" rel="shortcut icon">
        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/bootstrap.css" rel="stylesheet">
        <!-- Template CSS -->
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/animate.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/font-awesome.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/nexus.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/responsive.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url()?>assets/css/custom.css" rel="stylesheet">
        <!-- Google Fonts-->
        <link href="http://fonts.googleapis.com/css?family=Roboto+Condensed:400,300" rel="stylesheet" type="text/css">



        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <title>Expanding Search Bar Deconstructed</title>
        <meta name="description" content="Expanding Search Bar Deconstructed" />
        <meta name="keywords" content="transition, search, expanding, search input, sliding input, css3, javascript" />
        <meta name="author" content="Codrops" />
        <link rel="shortcut icon" href="../favicon.ico"> 
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/default.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/component.css" />
        <script src="<?php echo base_url()?>assets/js/modernizr.custom.js"></script>




    </head>
    <body>
        <div id="body-bg">
            <!-- Header -->
            <div id="header">
                <div class="container">
                    <div class="row">
                        <!-- Logo -->
                        <div class="logo">
                            <a href="<?php echo base_url()?>index.html" title="">
                                <img src="<?php echo base_url()?>assets/img/c.png" alt="Logo" />
                            </a>
                        </div>
                        <!-- End Logo -->
                    </div>
                </div>
            </div>
            <!-- End Header -->
            <!-- Top Menu -->
            <div id="hornav" class="bottom-border-shadow">
                <div class="container no-padding border-bottom">
                    <div class="row">
                        <div class="col-md-10 no-padding">
                            <div class="visible-lg">
                                <ul id="hornavmenu" class="nav navbar-nav">
                                    <li>
                                        <a href="<?php echo base_url()?>index.php" class="fa-home active">Beranda</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url()?>index.php#content" class="fa-gears ">Tentang Carica</a>
                                        <ul>
                                            <a href="<?php echo base_url()?>index.php#content" class="fa-gears ">Manfaat Carica</a>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url()?>index.php/welcome/produk" class="fa-copy ">Produk Carica</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url()?>index.php/welcome/carabeli" class="fa-th ">Cara Beli</a>
                                    </li>
                                    <li>
                                        <a href="contact.html" class="fa-comment ">Hubungi Kami</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-2 no-padding" style="text-align: right">
                            <!-- Top Navigation -->
           
                <!-- Optional columns for small components -->
                
                <div class="column">
                    <div id="sb-search" class="sb-search">
                        <form>
                            <input class="sb-search-input" placeholder="Enter your search term..." type="text" value="" name="search" id="search">
                            <input class="sb-search-submit" type="submit" value="">
                            <span class="sb-icon-search"></span>
                        </form>
                    </div>
                </div>
            
        <!-- /container -->
        <script src="<?php echo base_url()?>assets/js/classie.js"></script>
        <script src="<?php echo base_url()?>assets/js/uisearch.js"></script>
        <script>
            new UISearch( document.getElementById( 'sb-search' ) );
        </script>

                                </div>
                    </div>
                </div>
            </div>