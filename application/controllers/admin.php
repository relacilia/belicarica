<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	public function listproduk()
	{
		$data['query'] = $this->admin_m->get_allimage(); 
		$this->load->view('adminz/header');
		$this->load->view('adminz/list', $data);
		$this->load->view('adminz/footer');
	}

	public function listmerk()
	{
		$data['query'] = $this->admin_m->get_merk(); 
		$this->load->view('adminz/header');
		$this->load->view('adminz/listmerk', $data);
		$this->load->view('adminz/footer');
	}

	public function tambahproduk()
	{
		$data['query'] = $this->admin_m->get_merk();
		$this->load->view('adminz/header');
		$this->load->view('adminz/tambahp',$data);
		$this->load->view('adminz/footer');
	}

	public function editproduk($kode)
	{
		$data['pro'] = $this->admin_m->get_produk($kode)->row();
		$data['query'] = $this->admin_m->get_merk();
		$this->load->view('adminz/header');
		$this->load->view('adminz/editp', $data);
		$this->load->view('adminz/footer');
	}

	public function tambahmerk()
	{
		$this->load->view('adminz/header');
		$this->load->view('adminz/tambahm');
		$this->load->view('adminz/footer');
	}

	public function editmerk ($id)
	{
		$data['pre'] = $this->admin_m->get_merky($id)->row();
		// var_dump($data);
		// break;
		$this->load->view('adminz/header');
		$this->load->view('adminz/editm', $data);
		$this->load->view('adminz/footer');
	}

	public function topfive()
	{
		$data['query'] = $this->admin_m->get_top();
		$this->load->view('adminz/header');
		$this->load->view('adminz/top',$data);
		$this->load->view('adminz/footer');
	}

	public function dashboard()
	{
		$this->load->view('adminz/header');
		$this->load->view('adminz/dash');
		$this->load->view('adminz/footer');
	}

	public function hapus($kode)
	{
		$data ['query'] = $kode;
		$this->load->view('adminz/header');
		$this->load->view('adminz/hapus',$data);
		$this->load->view('adminz/footer');
	}
}