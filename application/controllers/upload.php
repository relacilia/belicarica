<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Upload extends CI_Controller {

	function do_upload()
	{
    $config['upload_path'] = './uploads/';
    $config['allowed_types'] = 'gif|jpg|png';
    $config['max_size'] = '1000';
    // $config['max_width']  = '';
    // $config['max_height']  = '';
    $config['overwrite'] = TRUE;
    $config['remove_spaces'] = TRUE;

    $this->load->library('upload', $config);

   

    $this->upload->do_upload('xyz');

    if ( ! $this->upload->do_upload('xyz'))
    {
        $error = array('error' => $this->upload->display_errors());

        $this->load->view('adminz/header');
        $this->load->view('adminz/tambahp');
        $this->load->view('adminz/footer');
    }
    else
    {
    	$gbr = $this->upload->data();
    	$data = array(
    		'Kode_Produk' => $this->input->post('kode'),
    		'Merk' => $this->input->post('merk'),
    		'Netto' => $this->input->post('netto'),
    		'Isi' => $this->input->post('isi'),
    		'Harga' => $this->input->post('harga'),
    		'Gambar' => $gbr['full_path'],
            'Nama_Gambar' => $gbr['file_name'],
            'Keterangan' => $this->input->post('ket')
    		);

    	
    	$this->admin_m->insert_images($data);

        // $data = array('upload_data' => $this->upload->data());
        $data2['query'] = $this->admin_m->get_allimage();
        $this->load->view('adminz/header');
        $this->load->view('adminz/list',$data2);
        $this->load->view('adminz/footer');
    	}
	}

	function do_update($kode)
	{
		$config['upload_path'] = './uploads/';
    	$config['allowed_types'] = 'gif|jpg|png';
    	$config['max_size'] = '1000';
    	// $config['max_width']  = '';
    	// $config['max_height']  = '';
    	$config['overwrite'] = TRUE;
    	$config['remove_spaces'] = TRUE;

    	$this->load->library('upload', $config);

    	$path ['pro']= $this->admin_m->get_path($kode);
        


    	if ($_FILES['xyz']['size'] == 0)
		{
			$data = array(
    		'Kode_Produk' => $kode,
    		'Merk' => $this->input->post('merk'),
    		'Netto' => $this->input->post('netto'),
    		'Isi' => $this->input->post('isi'),
    		'Harga' => $this->input->post('harga'),
    		'Gambar' => $path['pro']->Gambar,
            'Nama_Gambar' => $path['pro']->Nama_Gambar,
            'Keterangan' => $this->input->post('ket')
    		);
		}

		else
		{
			$this->upload->do_upload('xyz');
			$gbr = $this->upload->data();
			$data = array(
    		'Kode_Produk' => $kode,
    		'Merk' => $this->input->post('merk'),
    		'Netto' => $this->input->post('netto'),
    		'Isi' => $this->input->post('isi'),
    		'Harga' => $this->input->post('harga'),
    		'Gambar' => $gbr['full_path'],
            'Nama_Gambar' => $gbr['file_name'],
            'Keterangan' => $this->input->post('ket')
    		);

		}

		$res = $this->admin_m->updatedata($data,$kode);

		if ($res >=1) 
		{
			
			 $this->load->view('adminz/header');
			 $this->load->view('adminz/editp');
		     $this->load->view('adminz/footer');
		}
		else
		{
			$data['query'] = $this->admin_m->get_allimage();
			$this->load->view('adminz/header');
	        $this->load->view('adminz/list',$data);
	        $this->load->view('adminz/footer');

		}
	}

	function addmerk()
	{
		$merk = $this->input->post('merky');
		$res = $this->admin_m->tambahimerk($merk);

		if ($res >=1) 
		{
			
			 $this->load->view('adminz/header');
			 $this->load->view('adminz/tambahm');
		     $this->load->view('adminz/footer');
		}
		else
		{
			$data2['query'] = $this->admin_m->get_merk();
			$this->load->view('adminz/header');
	        $this->load->view('adminz/listmerk',$data2);
	        $this->load->view('adminz/footer');

		}
	}

    function update_merk($id)
    {
        $merk = array(
            'ID_Merk' => $id, 
            'Merk' => $this->input->post('merky')
            );
        
        $res = $this->admin_m->updatemerk($id,$merk);

        if ($res >=1) 
        {
            
             $this->load->view('adminz/header');
             $this->load->view('adminz/tambahm');
             $this->load->view('adminz/footer');
        }
        else
        {
            $data2['query'] = $this->admin_m->get_merk();
            $this->load->view('adminz/header');
            $this->load->view('adminz/listmerk',$data2);
            $this->load->view('adminz/footer');

        }
    }

    function topf()
    {
        $kode1 = $this->input->post('kode1');

        $data['fav'] = $this->admin_m->pencocokan($kode1);

        // $topfive = array(
        //     'Kode_Top' => '111',
        //     'Merk' => $data['fav']->Merk,
        //     'Netto' => $data['fav']->Netto,
        //     'Isi' => $data['fav']->Isi,
        //     'Harga' => $data['fav']->Harga,
        //     'Gambar' => $data['fav']->Gambar,
        //     'Nama_Gambar' => $data['fav']->Nama_Gambar
        //     );

        $Kode_Top = '1';
        $Kode_Produk = $data['fav']->Kode_Produk;
        $Merk = $data['fav']->Merk;
        $Netto = $data['fav']->Netto;
        $Isi = $data['fav']->Isi;
        $Harga = $data['fav']->Harga;
        $Gambar = $data['fav']->Gambar;
        $Nama_Gambar = $data['fav']->Nama_Gambar;
        $Keterangan = $data['fav']->Keterangan;

        $res= $this->admin_m->insert_top($Kode_Top,$Kode_Produk,$Merk,$Netto,$Isi,$Harga,$Gambar,$Nama_Gambar,$Keterangan);

        $kode1 = $this->input->post('kode2');

        $data['fav'] = $this->admin_m->pencocokan($kode1);


        $Kode_Top = '2';
        $Kode_Produk = $data['fav']->Kode_Produk;
        $Merk = $data['fav']->Merk;
        $Netto = $data['fav']->Netto;
        $Isi = $data['fav']->Isi;
        $Harga = $data['fav']->Harga;
        $Gambar = $data['fav']->Gambar;
        $Nama_Gambar = $data['fav']->Nama_Gambar;
        $Keterangan = $data['fav']->Keterangan;

        $res= $this->admin_m->insert_top($Kode_Top,$Kode_Produk,$Merk,$Netto,$Isi,$Harga,$Gambar,$Nama_Gambar,$Keterangan);

        $kode1 = $this->input->post('kode3');

        $data['fav'] = $this->admin_m->pencocokan($kode1);

        $Kode_Top = '3';
        $Kode_Produk = $data['fav']->Kode_Produk;
        $Merk = $data['fav']->Merk;
        $Netto = $data['fav']->Netto;
        $Isi = $data['fav']->Isi;
        $Harga = $data['fav']->Harga;
        $Gambar = $data['fav']->Gambar;
        $Nama_Gambar = $data['fav']->Nama_Gambar;
        $Keterangan = $data['fav']->Keterangan;

        $res= $this->admin_m->insert_top($Kode_Top,$Kode_Produk,$Merk,$Netto,$Isi,$Harga,$Gambar,$Nama_Gambar,$Keterangan);

        $kode1 = $this->input->post('kode4');

        $data['fav'] = $this->admin_m->pencocokan($kode1);

        $Kode_Top = '4';
        $Kode_Produk = $data['fav']->Kode_Produk;
        $Merk = $data['fav']->Merk;
        $Netto = $data['fav']->Netto;
        $Isi = $data['fav']->Isi;
        $Harga = $data['fav']->Harga;
        $Gambar = $data['fav']->Gambar;
        $Nama_Gambar = $data['fav']->Nama_Gambar;
        $Keterangan = $data['fav']->Keterangan;

        $res= $this->admin_m->insert_top($Kode_Top,$Kode_Produk,$Merk,$Netto,$Isi,$Harga,$Gambar,$Nama_Gambar,$Keterangan);

        $kode1 = $this->input->post('kode5');

        $data['fav'] = $this->admin_m->pencocokan($kode1);

        $Kode_Top = '5';
        $Kode_Produk = $data['fav']->Kode_Produk;
        $Merk = $data['fav']->Merk;
        $Netto = $data['fav']->Netto;
        $Isi = $data['fav']->Isi;
        $Harga = $data['fav']->Harga;
        $Gambar = $data['fav']->Gambar;
        $Nama_Gambar = $data['fav']->Nama_Gambar;
        $Keterangan = $data['fav']->Keterangan;

        $res= $this->admin_m->insert_top($Kode_Top,$Kode_Produk,$Merk,$Netto,$Isi,$Harga,$Gambar,$Nama_Gambar,$Keterangan);

        if ($res >=1) 
        {
            $data['query'] = $this->admin_m->get_top();
             $this->load->view('adminz/header');
             $this->load->view('adminz/top',$data);
             $this->load->view('adminz/footer');
        }
        else
        {
            $data['query'] = $this->admin_m->get_top();
            $this->load->view('adminz/header');
            $this->load->view('adminz/top',$data);
            $this->load->view('adminz/footer');

        }
    }

    function hapus ($kode)
    {
        $res = $this->admin_m->hapusdata($kode);
        if ($res >=1) 
        {
            $data['query'] = $this->admin_m->get_allimage();
            $this->load->view('adminz/header');
            $this->load->view('adminz/list',$data);
            $this->load->view('adminz/footer');
        }
        else
        {
            $data['query'] = $this->admin_m->get_allimage();
            $this->load->view('adminz/header');
            $this->load->view('adminz/list',$data);
            $this->load->view('adminz/footer');

        }

    }

}