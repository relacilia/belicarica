<?php 
defined('BASEPATH') OR exit ('No direct script access allowed');

class Admin_m extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	function insert_images($data){

      $this->db->insert('produk',$data);
	}

	function updatedata($data, $kode)
	{
		$this->db->where('Kode_Produk', $kode);
        $this->db->update('produk', $data);
	}

	function get_allimage() {
        $this->db->from('produk');
		$query = $this->db->get();

        //cek apakah ada data
        if ($query->num_rows() > 0) {
            return $query->result();
        }
	}

	function tambahimerk($data){

      $this->db->query("INSERT INTO Merk (merk) VALUES ('$data')");
	}

	function get_merk() {
        $this->db->from('merk');
		$query = $this->db->get();

        //cek apakah ada data
        if ($query->num_rows() > 0) {
            return $query->result();
        }
	}

	function get_produk($kode) {
        $data = $this->db->query("SELECT * FROM produk WHERE Kode_Produk = '$kode'");
        // $row = $data->row();
        return $data;
        //cek apakah ada data
        // if ($query->num_rows() > 0) {
        //     return $query->result();
        // }
	}

	function get_path($kode)
	{
		$data = $this->db->query("SELECT Gambar, Nama_Gambar FROM produk WHERE Kode_Produk = '$kode'");
		return $data->row();
	}

	function get_merky($id)
	{
		$data = $this->db->query("SELECT * FROM merk WHERE ID_Merk = '$id'");
        return $data;
	}

	function updatemerk($id, $merk)
	{
		$this->db->where('ID_Merk', $id);
        $this->db->update('merk', $merk);
	}

	function pencocokan($kode)
	{
		$data = $this->db->query("SELECT * FROM produk WHERE Kode_Produk = '$kode'");
        return $data->row();
	}

	function insert_top($Kode_Top,$Kode_Produk,$Merk,$Netto,$Isi,$Harga,$Gambar,$Nama_Gambar,$Keterangan)
	{
		//$this->db->query("INSERT INTO top (Kode_Top, Merk, Netto, Isi, Harga, Gambar, Nama_Gambar) VALUES ('$Kode_Top','$Merk','$Netto','$Isi','$Harga','$Gambar','$Nama_Gambar')");
		$this->db->query("UPDATE top SET Kode_Produk = '$Kode_Produk', Merk = '$Merk', Netto = '$Netto', Isi = '$Isi', Harga = '$Harga', Gambar = '$Gambar', Nama_Gambar = '$Nama_Gambar', Keterangan = '$Keterangan' WHERE ID_Top = '$Kode_Top'");
	}

	function get_top()
	{
		$this->db->from('top');
		$query = $this->db->get();

        //cek apakah ada data
        if ($query->num_rows() > 0) {
            return $query->result();
        }
	}

	function hapusdata($kode)
	{
		$res = $this->db->query("DELETE FROM produk where Kode_Produk = '$kode'");
		return $res;
	}




}


?>